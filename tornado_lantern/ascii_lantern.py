# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, division

import sys


class AsciiLantern(object):

    def __init__(self, is_on=False, color='#FFFFFF', printer=print):
        self.color = color
        self.is_on = is_on
        self._print = printer
        self._line = ''
        self._redraw()

    def switch_on(self):
        self.is_on = True
        self._redraw()

    def switch_off(self):
        self.is_on = False
        self._redraw()

    def set_color(self, r, g, b):
        assert 0 <= r < 256
        assert 0 <= g < 256
        assert 0 <= b < 256
        self.color = '#{:02X}{:02X}{:02X}'.format(r, g, b)
        self._redraw()

    def _render(self):
        if self.is_on:
            return '[ON: {}]'.format(self.color)
        else:
            return '[OFF]'

    def _redraw(self):
        self._print(self._render())


class StatusLinePrinter(object):
    def __init__(self):
        self._line = ''

    def __call__(self, new_line):
        delta = len(self._line) - len(new_line)
        self._line = new_line

        sys.stdout.write(new_line)
        if delta > 0:
            sys.stdout.write(' ' * delta)
        sys.stdout.write('\r')
        sys.stdout.flush()
