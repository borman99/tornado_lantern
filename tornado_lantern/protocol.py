# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, division

TAG_ON = 0x12
TAG_OFF = 0x13
TAG_COLOR = 0x20


def make_tlv(tag, payload=''):
    assert 0 <= len(payload) < 65536
    assert 0 <= tag < 256
    return ''.join((
        chr(tag),
        chr(len(payload) // 256),
        chr(len(payload) % 256),
        payload,
    ))


def make_color(r, g, b):
    assert 0 <= r < 256
    assert 0 <= g < 256
    assert 0 <= b < 256
    return chr(r) + chr(g) + chr(b)
