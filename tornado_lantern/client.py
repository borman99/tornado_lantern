# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, division

import abc
import logging

import tornado.gen

from . import protocol

logger = logging.getLogger(__name__)


class TLVClientBase(object):
    """Abstract TLV protocol client"""
    __metaclass__ = abc.ABCMeta

    def __init__(self, iostream):
        self._iostream = iostream

    @abc.abstractmethod
    def on_message(self, tag, payload):
        pass

    @tornado.gen.coroutine
    def run(self):
        while True:
            tag, payload = yield self.read_message()
            self.on_message(tag, payload)

    @tornado.gen.coroutine
    def read_message(self):
        # FIXME: Incomplete read is handled as end of stream

        # read TLV header: tag (1 byte) + length (2 bytes)
        header = yield self._iostream.read_bytes(3)
        assert len(header) == 3
        tag, l1, l2 = map(ord, header)
        # TLV length is big endian
        payload_length = (l1 << 8) + l2

        # only read body if required to
        if payload_length == 0:
            payload = ''
        else:
            payload = yield self._iostream.read_bytes(payload_length)
            assert len(payload) == payload_length

        raise tornado.gen.Return((tag, payload))


class LanternClient(TLVClientBase):
    def __init__(self, iostream, lantern):
        super(LanternClient, self).__init__(iostream)
        self._lantern = lantern
        self._handlers = {
            protocol.TAG_ON: self.command_on,
            protocol.TAG_OFF: self.command_off,
            protocol.TAG_COLOR: self.command_color,
        }

    def on_message(self, tag, payload):
        handler = self._handlers.get(tag)
        if handler is not None:
            handler(payload)
        else:
            # ignore unknown commands
            logger.warning(
                "Got unknown command %d: %s",
                tag,
                payload.encode('hex'),
            )

    def command_on(self, _):
        logger.info("Got ON")
        self._lantern.switch_on()

    def command_off(self, _):
        logger.info("Got OFF")
        self._lantern.switch_off()

    def command_color(self, rgb):
        logger.info("Got COLOR(#%s)", rgb.encode('hex'))
        assert len(rgb) == 3
        r, g, b = map(ord, rgb)
        self._lantern.set_color(r, g, b)
