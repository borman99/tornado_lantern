# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, division

import logging
import random

import tornado.gen
from tornado.ioloop import IOLoop
from tornado.tcpserver import TCPServer
from tornado.options import define, options

from .protocol import (
    make_tlv, make_color,
    TAG_ON, TAG_OFF, TAG_COLOR,
)

logger = logging.getLogger(__name__)


def random_color():
    return make_color(
        r=random.randint(0, 255),
        g=random.randint(0, 255),
        b=random.randint(0, 255),
    )


def random_message():
    dice = random.random()
    if 0 <= dice < 0.2:
        return make_tlv(TAG_ON)
    elif 0.2 <= dice < 0.4:
        return make_tlv(TAG_OFF)
    elif 0.4 <= dice < 1:
        return make_tlv(TAG_COLOR, random_color())


class LanternServer(TCPServer):

    @tornado.gen.coroutine
    def handle_stream(self, stream, address):
        logger.info("New client: %s", address)
        try:
            while True:
                yield stream.write(random_message())
                yield tornado.gen.sleep(1)
        except tornado.iostream.StreamClosedError:
            logger.info("Client disconnected: %s", address)


def main():
    define('host',
           default='127.0.0.1',
           help="Lantern server host")
    define('port',
           default=9999,
           type=int,
           help="Lantern server port")

    tornado.options.parse_command_line()
    server = LanternServer()
    server.listen(options.port)
    IOLoop.current().start()


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        logger.warning("Terminated by SIGINT")
