# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, division

from datetime import timedelta
from functools import partial
import logging

import tornado.gen
from tornado.ioloop import IOLoop
from tornado.tcpclient import TCPClient
from tornado.options import define, options

from .client import LanternClient
from .ascii_lantern import AsciiLantern, StatusLinePrinter

logger = logging.getLogger(__name__)


@tornado.gen.coroutine
def run(host, port, reconnect_timeout):
    lantern = AsciiLantern(printer=StatusLinePrinter())
    tcp = TCPClient()
    while True:
        try:
            logger.debug("Connecting to %s:%d...", host, port)
            iostream = yield tcp.connect(host, port)
        except Exception:
            logger.exception("Failed to connect.")
            yield tornado.gen.sleep(reconnect_timeout)
            continue

        logger.info("Connected to %s:%d...", host, port)
        client = LanternClient(iostream, lantern)
        try:
            yield client.run()
        except Exception:
            logger.exception("Connection aborted.")
            yield tornado.gen.sleep(reconnect_timeout)
            continue

        break


def main():
    define('host',
           default='127.0.0.1',
           help="Lantern server host")
    define('port',
           default=9999,
           type=int,
           help="Lantern server port")
    define('reconnect_timeout',
           default=timedelta(seconds=5),
           type=timedelta,
           help="Reconnection timeout")

    tornado.options.parse_command_line()
    IOLoop.current().run_sync(
        partial(
            run,
            options.host,
            options.port,
            options.reconnect_timeout.total_seconds(),
        )
    )


if __name__ == '__main__':
    try:
        main()
    except KeyboardInterrupt:
        logger.warning("Terminated by SIGINT")
