# -*- coding: utf-8 -*-

from __future__ import absolute_import, print_function, division

import os
from threading import Thread

import tornado.gen
from tornado.ioloop import IOLoop
from tornado.iostream import PipeIOStream, StreamClosedError

from tornado_lantern.client import LanternClient
from tornado_lantern.protocol import (
    make_tlv, make_color,
    TAG_ON, TAG_OFF, TAG_COLOR
)


class MockLantern(object):

    def __init__(self, is_on=False, color=(0, 0, 0)):
        self.color = color
        self.is_on = is_on

    def switch_on(self):
        self.is_on = True

    def switch_off(self):
        self.is_on = False

    def set_color(self, r, g, b):
        self.color = (r, g, b)


def send_commands(lantern, commands):
    read_fd, write_fd = os.pipe()

    def producer():
        for tag, payload in commands:
            os.write(write_fd, make_tlv(tag, payload))
        os.close(write_fd)

    @tornado.gen.coroutine
    def consumer():
        iostream = PipeIOStream(read_fd)
        client = LanternClient(iostream, lantern)
        try:
            yield client.run()
        except StreamClosedError:
            pass

    producer_thread = Thread(target=producer)
    producer_thread.start()

    IOLoop.current().run_sync(consumer)

    producer_thread.join()


def test_lantern_client():
    lantern = MockLantern()

    assert lantern.is_on is False
    assert lantern.color == (0, 0, 0)

    # Switch on
    send_commands(lantern, [
        (TAG_ON, '')
    ])

    assert lantern.is_on is True
    assert lantern.color == (0, 0, 0)

    # Change color
    send_commands(lantern, [
        (TAG_COLOR, make_color(0x12, 0x34, 0x56))
    ])

    assert lantern.is_on is True
    assert lantern.color == (0x12, 0x34, 0x56)

    # Switch off
    send_commands(lantern, [
        (TAG_OFF, '')
    ])

    assert lantern.is_on is False
    assert lantern.color == (0x12, 0x34, 0x56)

    # Send some garbage
    send_commands(lantern, [
        (0xFF, 'foobar!')
    ])

    assert lantern.is_on is False
    assert lantern.color == (0x12, 0x34, 0x56)
