# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(
    name='tornado_lantern',
    version='1.0',
    author='Mikhail Borisov',
    packages=find_packages(),
    install_requires=['tornado'],
)
